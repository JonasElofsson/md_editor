﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace MD
{
    public class MDSE11
    {
        string ComPort;
        SerialPort SP = null;
        int RState = 0;
        int RCount;
        Packet RPacket;

        List<Packet> RPackets = new List<Packet>();

        public delegate void ElapsedTime_delegate(byte Track, byte Minute, byte Second);
        public ElapsedTime_delegate ElapsedTime;

        public delegate void StatusData_delegate(StatusData SD);
        public StatusData_delegate Status_Data;


        public class ModelData
        {
            public bool TimeMachineAvailable;
            public bool RecordingAvailable;
        }

        public enum State_ { STOP, PLAY, PAUSE, EJECT, REC_PLAY, REC_PAUSE, REHERSAL, RESERVED, NOT_AVAILABLE_TO_PLAY };
        public enum Source_ { Analog, Optical, Coaxial, Reserved };
        public enum DiscType_ { Reserved, Recordable, Pre_master };

        bool Hijack_StatusData = false;


        public class StatusData
        {
            public bool Disc_inserted;
            public bool Power_on;
            public bool TOC_read;
            public bool REC_possible;
            public bool Stereo;
            public bool Copy_possible;
            public bool DIN_lock;
            public State_ State;
            public Source_ Source;
        }

        public class DiscData
        {
            public bool DiscError;
            public bool Protected;
            public DiscType_ DiscType;
        }

        public class Packet
        {
            public byte[] Data = new byte[32];
        }

        public class RecDateData
        {
            public int Track;
            public int Year;
            public int Month;
            public int Day;
            public int Hour;
            public int Min;
            public int Sec;
        }

        public class TOCData
        {
            public int FirstTrackNo;
            public int LastTrackNo;
            public int RecordedMin;
            public int RecordedSec;
        }

        public class TimeData
        {
            public int Min;
            public int Sec;
        }

        public class AllNameData
        {
            public string DiscName;
            public List<string> TrackNames;
        }

        public MDSE11(string Comport)
        {
            ComPort = Comport;

            SP = new SerialPort(Comport, 9600, Parity.None, 8, StopBits.One);
            SP.ReadBufferSize = 100000;
            SP.Open();
            SP.DataReceived += GotData;
        }

        public void DisConnect()
        {
            try
            {
                SP.Close();
            }
            catch
            { }
        }

        public Packet WaitPacket(int id, int timeout)
        {
            do
            {
                for (int i = 0; i < RPackets.Count; i++)
                {
                    if (RPackets[i].Data[4] == id)
                    {
                        Packet Re = RPackets[i];
                        RPackets.RemoveAt(i);
                        return Re;
                    }
                }
                timeout -= 20;
                System.Threading.Thread.Sleep(20);
            } while (timeout >= 0);

            Packet R = new Packet();
            return R;
        }

        public Packet WaitPacket(int id, int id2, int timeout)
        {
            do
            {
                for (int i = 0; i < RPackets.Count; i++)
                {
                    if (RPackets[i].Data[4] == id && RPackets[i].Data[5] == id2)
                    {
                        Packet Re = RPackets[i];
                        RPackets.RemoveAt(i);
                        return Re;
                    }
                }
                timeout -= 20;
                System.Threading.Thread.Sleep(20);
            } while (timeout >= 0);

            Packet R = new Packet();
            return R;
        }

        public Packet WaitPacket(int id, int id2, int id3, int timeout)
        {
            do
            {
                for (int i = 0; i < RPackets.Count; i++)
                {
                    if (RPackets[i].Data[4] == id && RPackets[i].Data[5] == id2 && RPackets[i].Data[6] == id3)
                    {
                        Packet Re = RPackets[i];
                        RPackets.RemoveAt(i);
                        return Re;
                    }
                }
                timeout -= 20;
                System.Threading.Thread.Sleep(20);
            } while (timeout >= 0);

            Packet R = new Packet();
            return R;
        }

        public Packet WaitPacket(int id, int id2, int id3, int id4, int timeout)
        {
            do
            {
                for (int i = 0; i < RPackets.Count; i++)
                {
                    if (RPackets[i].Data[4] == id && RPackets[i].Data[5] == id2 && RPackets[i].Data[6] == id3 && RPackets[i].Data[7] == id4)
                    {
                        Packet Re = RPackets[i];
                        RPackets.RemoveAt(i);
                        return Re;
                    }
                }
                timeout -= 20;
                System.Threading.Thread.Sleep(20);
            } while (timeout >= 0);

            Packet R = new Packet();
            return R;
        }

        public void ClearPackets()
        {
            RPackets.Clear();
        }

        private void GotData(object sender, SerialDataReceivedEventArgs e)
        {
            while(SP.BytesToRead > 0)
            {
                HandleByte(SP.ReadByte());
            }
        }

        private void HandleByte(int data)
        {
            Console.Write("0x{00:X} ", data);
            switch (RState)
            {
                case 0:
                    if (data == 0x6F) //Header
                    {
                        RCount = 0;
                        RPacket = new Packet();
                        RPacket.Data[RCount] = (byte)data;
                        RCount++;
                        RState = 1;
                    }
                    break;
                case 1: //Packet length
                    RPacket.Data[RCount] = (byte)data;
                    RCount++;
                    RState = 2;
                    break;
                case 2: //Format type
                    RPacket.Data[RCount] = (byte)data;
                    RCount++;
                    RState = 3;
                    break;
                case 3: //Category
                    RPacket.Data[RCount] = (byte)data;
                    RCount++;
                    RState = 4;
                    break;
                case 4: //Data
                    RPacket.Data[RCount] = (byte)data;
                    RCount++;
                    if (data == 0xff)
                    {
                        RState = 0;
                        Complete(RPacket);
                        Console.WriteLine(" ");
                    }
                    break;
            }
        }

        void Complete(Packet P)
        {
            //Is this a Elapsed Time packet?

            if (P.Data[4] == 0x20 && P.Data[5] == 0x51)
            {
                ElapsedTime.Invoke(P.Data[6], P.Data[8], P.Data[9]);
            }
            else if(P.Data[4] == 0x20 && P.Data[5] == 0x20 && Hijack_StatusData == false) //Status data?
            {
                if(Status_Data != null) Status_Data.Invoke(ParseStatusData(P));
            }
            else
            {
                RPackets.Add(P);
            }
        }

        private void SendPacket(Packet P)
        {
            SP.Write(P.Data, 0, (int)P.Data[1]);
        }


        //Commands
        public int RemoteMode(int onoff)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x10;
            P.Data[5] = 0x03;
            if (onoff == 0) P.Data[5] = 0x04;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x10, 500);
            if (R == null) return -1;
            if (R.Data[5] == 0x03) return 1;
            return 0;
        }

        public int Power(int onoff)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x01;
            P.Data[5] = 0x02;
            if (onoff == 0) P.Data[5] = 0x03;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x01, 500);
            if (R == null) return -1;
            if (R.Data[5] == 0x02) return 1;
            return 0;
        }

        public int Play()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x01;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 0x01, 500);
            if (R == null) return -1;
            return 1;
        }

        public int Stop()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x02;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 0x02, 500);
            if (R == null) return -1;
            return 1;
        }

        public int PauseOnOff()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x03;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 500);
            if (R == null) return -1;
            return 1;
        }

        public int PauseOn()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x06;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 0x03, 500);
            if (R == null) return -1;
            return 1;
        }

        public int FF_REW_OFF()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x06;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x00;
            P.Data[5] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x00, 500);
            if (R == null) return -1;
            return 1;
        }

        public int Rew()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x13;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 500);
            if (R == null) return -1;
            return 1;
        }

        public int FF()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x14;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 500);
            if (R == null) return -1;
            return 1;
        }

        public int Prev_Track()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x15;
            P.Data[6] = 0xff;

            SendPacket(P);

            //Packet R = WaitPacket(0x02, 500);
            //if (R == null) return -1;
            return 1;
        }

        public int Next_Track()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x16;
            P.Data[6] = 0xff;

            SendPacket(P);

            //Packet R = WaitPacket(0x02, 500);
            //if (R == null) return -1;
            return 1;
        }

        public int Rec()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x21;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 0x21, 500);
            if (R == null) return -1;
            return 1;
        }

        public int Time_Machine_Rec()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x28;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 500);
            if (R == null) return -1;
            return 1;
        }

        public int Eject()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x40;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 0x40, 1500);
            if (R == null) return -1;
            return 1;
        }

        public int Auto_Pause(int onoff)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x02;
            P.Data[5] = 0x80;
            if(onoff == 1) P.Data[5] = 0x81;
            P.Data[6] = 0xff;

            SendPacket(P);

            //Packet R = WaitPacket(0x02, 5000);
            //if (R == null) return -1;
            return 1;
        }

        public int Track_Play(int Track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x09;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x03;
            P.Data[5] = 0x42;
            P.Data[6] = 0x01;
            P.Data[7] = (byte)Track;
            P.Data[8] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 0x01, 5000);
            if (R == null) return -1;
            return 1;
        }

        public int Track_Pause(int Track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x09;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x03;
            P.Data[5] = 0x43;
            P.Data[6] = 0x01;
            P.Data[7] = (byte)Track;
            P.Data[8] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 0x03, 5000);
            if (R == null) return -1;
            return 1;
        }

        public int ElapsedTimeOnOff(int onoff)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x07;
            P.Data[5] = 0x11;
            if (onoff == 1) P.Data[5] = 0x10;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x02, 500);
            if (R == null) return -1;
            return 1;
        }

        public int DivideModeReq()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x01;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 1500);
            if (R == null) return -1;
            return 1;
        }

        public int DivideAdjust(int Position)
        {
            ClearPackets();

            if (Position > 127) Position = 127;
            if (Position < -128) Position = -128;

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x08;
            P.Data[6] = (byte)Position;
            P.Data[7] = 0xff;

            SendPacket(P);

            //Packet R = WaitPacket(0x0A, 5000);
            //if (R == null) return -1;
            return 1;
        }

        public int DivideReq()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x02;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 5000);
            if (R == null) return -1;
            return 1;
        }
        //To combine 2 tracks, call Mode first with the second track, then Combine with the second + 1
        public int CombineModeReqE11(int track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x06;
            P.Data[6] = (byte)track;
            P.Data[7] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 500);
            if (R == null) return -1;
            return 1;
        }

        public int CombineReqE11(int track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x07;
            P.Data[6] = (byte)track;
            P.Data[7] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 5000);
            if (R == null) return -1;
            return 1;
        }

        public int EditModeCancel()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x03;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 500);
            if (R == null) return -1;
            return 1;
        }

        public int Erase(int track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x04;
            P.Data[6] = (byte)track;
            P.Data[7] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 5000);
            if (R == null) return -1;
            return 1;
        }

        public int Move(int FromTrack, int ToTrack)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x09;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x05;
            P.Data[6] = (byte)FromTrack;
            P.Data[7] = (byte)ToTrack;
            P.Data[8] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 2500);
            if (R == null) return -1;
            return 1;
        }

        public int Undo()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x0A;
            P.Data[5] = 0x11;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x0A, 1500);
            if (R == null) return -1;
            return 1;
        }

        public ModelData ModelRequest()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x10;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 500);
            if (R == null) return null;

            ModelData ModelD = new ModelData();
            ModelD.RecordingAvailable = false;
            ModelD.TimeMachineAvailable = false;
            if ((R.Data[7] & 0x02) != 0) ModelD.TimeMachineAvailable = true;
            if ((R.Data[7] & 0x01) != 0) ModelD.RecordingAvailable = true;
            return ModelD;

        }

        public StatusData StatusRequest()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x20;
            P.Data[6] = 0xff;

            Hijack_StatusData = true;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 500);

            Hijack_StatusData = false;

            if (R == null) return null;

            return ParseStatusData(R);
        }

        public DiscData DiscDataRequest()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x21;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 500);
            if (R == null) return null;

            DiscData DD = new DiscData();
            
            if ((R.Data[7] & 0x08) != 0) DD.DiscError = true;
            if ((R.Data[7] & 0x04) != 0) DD.Protected = true;
            switch(R.Data[7] & 0x03)
            {
                case 1: DD.DiscType = DiscType_.Recordable; break;
                case 2: DD.DiscType = DiscType_.Pre_master; break;
                default: DD.DiscType = DiscType_.Reserved; break;
            }
            
            return DD;
        }

        public string ModelNameRequest()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x07;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x22;
            P.Data[6] = 0xff;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 0x22, 500);
            if (R == null) return null;

            return System.Text.Encoding.ASCII.GetString(R.Data, 6, 14);
        }

        public RecDateData RecDateRequest(int Track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x24;
            P.Data[6] = (byte)Track;
            P.Data[7] = 0xFF;
            
            SendPacket(P);

            Packet R = WaitPacket(0x20, 500);
            if (R == null) return null;

            RecDateData RCD = new RecDateData();
            RCD.Track = (int)(R.Data[6]);
            RCD.Year = (int)(R.Data[7]);
            RCD.Month = (int)(R.Data[8]);
            RCD.Day = (int)(R.Data[9]);
            RCD.Hour = (int)(R.Data[10]);
            RCD.Min = (int)(R.Data[11]);
            RCD.Sec = (int)(R.Data[12]);

            if (RCD.Year > 90) RCD.Year += 1900;
            else RCD.Year += 2000;

            return RCD;
        }

        public TOCData TocDataRequest()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x44;
            P.Data[6] = 0x01;
            P.Data[7] = 0xFF;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 500);
            if (R == null) return null;

            TOCData TD = new TOCData();
            TD.FirstTrackNo = (int)(R.Data[7]);
            TD.LastTrackNo = (int)(R.Data[8]);
            TD.RecordedMin = (int)(R.Data[9]);
            TD.RecordedSec = (int)(R.Data[10]);
            
            return TD;
        }

        public TimeData TrackTimeRequest(int Track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x09;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x45;
            P.Data[6] = 0x01;
            P.Data[7] = (byte)(Track);
            P.Data[8] = 0xFF;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 0x62, 500);
            if (R == null) return null;

            TimeData TTD = new TimeData();
            TTD.Min = (int)(R.Data[8]);
            TTD.Sec = (int)(R.Data[9]);
            
            return TTD;
        }

        public string DiscNameRequest()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x48;
            P.Data[6] = 0x01;
            P.Data[7] = 0xFF;

            SendPacket(P);
            
            Packet R = WaitPacket(0x20, 0x48, 0x01, 500);
            if (R == null) return null;

            string Name = System.Text.Encoding.ASCII.GetString(R.Data, 7, 16);
            int Pnr = 2;
            while (R.Data[22] != 0)
            {
                R = WaitPacket(0x20, 0x49, (byte)Pnr, 500);
                Name += System.Text.Encoding.ASCII.GetString(R.Data, 7, 16);
                Pnr++;
            }

            return Name;
        }

        public string TrackNameRequest(int Track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x4A;
            P.Data[6] = (byte)Track;
            P.Data[7] = 0xFF;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 0x4A, (byte)Track, 500);
            if (R == null) return null;

            string Name = System.Text.Encoding.ASCII.GetString(R.Data, 7, 16);
            int Pnr = 2;
            while (R.Data[22] != 0)
            {
                R = WaitPacket(0x20, 0x4B, (byte)Pnr, 500);
                Name += System.Text.Encoding.ASCII.GetString(R.Data, 7, 16);
                Pnr++;
            }

            return Name;
        }

        public TimeData RecRemainRequest()
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x08;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x54;
            P.Data[6] = 0x01;
            P.Data[7] = 0xFF;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 0x54, 0x01, 500);
            if (R == null) return null;

            TimeData TD = new TimeData();
            TD.Min = R.Data[7];
            TD.Sec = R.Data[8];

            return TD;
        }

        public int NameRemainRequest(int Track)
        {
            ClearPackets();

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x09;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x55;
            P.Data[6] = 0x00;
            P.Data[7] = (byte)Track;
            P.Data[8] = 0xFF;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 0x55, 0x00, Track, 500);
            if (R == null) return -1;

            int Space = ((int)(R.Data[8]) * 256) + (int)R.Data[9];

            return Space;
        }

        byte[] PrepareByteString(byte[] indata, int startPos)
        {
            byte[] outData = new byte[16];

            for (int i = 0; i < 16; i++)
            {
                outData[i] = 0;
            }

            for(int i = 0; i < 16; i++)
            {
                if(startPos + i < indata.Length)
                {
                    outData[i] = indata[startPos + i];
                }
            }

            return outData;
        }

        public int DiscNameWrite(string Name)
        {
            ClearPackets();

            byte[] Textbytes = Encoding.ASCII.GetBytes(Name);

            byte[] Data = PrepareByteString(Textbytes, 0);

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x18;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x70;
            P.Data[6] = 0x01;

            for (int i = 0; i < 16; i++)
            {
                P.Data[7 + i] = Data[i];
            }

            P.Data[23] = 0xFF;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 0x87, 2500);
            if (R == null) return -1;

            int Pno = 2;
            int Pos = 16;

            while(Data[15] != 0)
            {
                Data = PrepareByteString(Textbytes, Pos);

                P.Data[5] = 0x71;
                P.Data[6] = (byte)Pno;

                for (int i = 0; i < 16; i++)
                {
                    P.Data[7 + i] = Data[i];
                }

                P.Data[23] = 0xFF;

                SendPacket(P);

                R = WaitPacket(0x20, 0x87, 10000);
                if (R == null) return -1;

                Pos += 16;
                Pno++;
            }

            System.Threading.Thread.Sleep(2000); //Give the deck time to breath!

            return 0;
        }

        public int TrackNameWrite(int Track, string Name)
        {
            ClearPackets();

            byte[] Textbytes = Encoding.ASCII.GetBytes(Name);

            byte[] Data = PrepareByteString(Textbytes, 0);

            Packet P = new Packet();
            P.Data[0] = 0x7e;
            P.Data[1] = 0x18;
            P.Data[2] = 0x05;
            P.Data[3] = 0x47;
            P.Data[4] = 0x20;
            P.Data[5] = 0x72;
            P.Data[6] = (byte)Track;

            for (int i = 0; i < 16; i++)
            {
                P.Data[7 + i] = Data[i];
            }

            P.Data[23] = 0xFF;

            SendPacket(P);

            Packet R = WaitPacket(0x20, 0x87, 2500);
            if (R == null) return -1;

            int Pno = 2;
            int Pos = 16;

            while (Data[15] != 0)
            {
                Data = PrepareByteString(Textbytes, Pos);

                P.Data[5] = 0x73;
                P.Data[6] = (byte)Pno;

                for (int i = 0; i < 16; i++)
                {
                    P.Data[7 + i] = Data[i];
                }

                P.Data[23] = 0xFF;

                SendPacket(P);

                R = WaitPacket(0x20, 0x87, 10000);
                if (R == null) return -1;

                Pos += 16;
                Pno++;
            }

            System.Threading.Thread.Sleep(2000); //Give the deck time to breath!

            return 0;
        }


        StatusData ParseStatusData(Packet P)
        {
            StatusData SD = new StatusData();
            //[6] = Data1
            if ((P.Data[6] & 0x20) != 0) SD.Disc_inserted = false; else SD.Disc_inserted = true;
            if ((P.Data[6] & 0x10) != 0) SD.Power_on = false; else SD.Power_on = true;

            switch (P.Data[6] & 0x0F)
            {
                case 0:
                    SD.State = State_.STOP;
                    break;
                case 1:
                    SD.State = State_.PLAY;
                    break;
                case 2:
                    SD.State = State_.PAUSE;
                    break;
                case 3:
                    SD.State = State_.EJECT;
                    break;
                case 4:
                    SD.State = State_.REC_PLAY;
                    break;
                case 5:
                    SD.State = State_.REC_PAUSE;
                    break;
                case 6:
                    SD.State = State_.REHERSAL;
                    break;
                case 15:
                    SD.State = State_.NOT_AVAILABLE_TO_PLAY;
                    break;
                default:
                    SD.State = State_.RESERVED;
                    break;
            }

            if ((P.Data[7] & 0x80) != 0) SD.TOC_read = true; else SD.TOC_read = false;
            if ((P.Data[7] & 0x20) != 0) SD.REC_possible = true; else SD.REC_possible = false;

            if ((P.Data[8] & 0x80) != 0) SD.Stereo = true; else SD.Stereo = false;
            if ((P.Data[8] & 0x40) != 0) SD.Copy_possible = false; else SD.Copy_possible = true;
            if ((P.Data[8] & 0x20) != 0) SD.DIN_lock = false; else SD.DIN_lock = true;

            switch (P.Data[8] & 0x07)
            {
                case 1:
                    SD.Source = Source_.Analog;
                    break;
                case 3:
                    SD.Source = Source_.Optical;
                    break;
                case 5:
                    SD.Source = Source_.Coaxial;
                    break;
                default:
                    SD.Source = Source_.Reserved;
                    break;
            }

            return SD;
        }


    }
}

//            return System.Text.Encoding.ASCII.GetString(R.Data, 6, 14);
