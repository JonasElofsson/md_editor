﻿namespace MD_Application
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComPortSelector = new System.Windows.Forms.ComboBox();
            this.bConnect = new System.Windows.Forms.Button();
            this.bDisconnect = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gTracks = new System.Windows.Forms.GroupBox();
            this.TrackScrollBar = new System.Windows.Forms.VScrollBar();
            this.lDiscInfo = new System.Windows.Forms.Label();
            this.bWriteDiscTitle = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bReadTOC = new System.Windows.Forms.Button();
            this.tbDiscTitle = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bDeleteAfter = new System.Windows.Forms.Button();
            this.bDeleteBefore = new System.Windows.Forms.Button();
            this.bDivideHere = new System.Windows.Forms.Button();
            this.bDivide = new System.Windows.Forms.Button();
            this.lCut = new System.Windows.Forms.Label();
            this.sbCut = new System.Windows.Forms.HScrollBar();
            this.bPlay = new System.Windows.Forms.Button();
            this.bStop = new System.Windows.Forms.Button();
            this.bPause = new System.Windows.Forms.Button();
            this.bFFwd = new System.Windows.Forms.Button();
            this.bRew = new System.Windows.Forms.Button();
            this.bPrevTrack = new System.Windows.Forms.Button();
            this.bNextTrack = new System.Windows.Forms.Button();
            this.bRecord = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lTrackInfo = new System.Windows.Forms.Label();
            this.bEject = new System.Windows.Forms.Button();
            this.bWriteTOC = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gTracks.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ComPortSelector
            // 
            this.ComPortSelector.FormattingEnabled = true;
            this.ComPortSelector.Location = new System.Drawing.Point(30, 25);
            this.ComPortSelector.Name = "ComPortSelector";
            this.ComPortSelector.Size = new System.Drawing.Size(67, 21);
            this.ComPortSelector.TabIndex = 0;
            // 
            // bConnect
            // 
            this.bConnect.Location = new System.Drawing.Point(103, 24);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(75, 23);
            this.bConnect.TabIndex = 1;
            this.bConnect.Text = "Connect";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // bDisconnect
            // 
            this.bDisconnect.Location = new System.Drawing.Point(183, 24);
            this.bDisconnect.Name = "bDisconnect";
            this.bDisconnect.Size = new System.Drawing.Size(75, 23);
            this.bDisconnect.TabIndex = 2;
            this.bDisconnect.Text = "Disconnect";
            this.bDisconnect.UseVisualStyleBackColor = true;
            this.bDisconnect.Click += new System.EventHandler(this.bDisconnect_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(697, 390);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.bWriteTOC);
            this.tabPage1.Controls.Add(this.gTracks);
            this.tabPage1.Controls.Add(this.lDiscInfo);
            this.tabPage1.Controls.Add(this.bWriteDiscTitle);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.bReadTOC);
            this.tabPage1.Controls.Add(this.tbDiscTitle);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(689, 364);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Titles";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gTracks
            // 
            this.gTracks.Controls.Add(this.TrackScrollBar);
            this.gTracks.Location = new System.Drawing.Point(25, 73);
            this.gTracks.Name = "gTracks";
            this.gTracks.Size = new System.Drawing.Size(642, 285);
            this.gTracks.TabIndex = 5;
            this.gTracks.TabStop = false;
            this.gTracks.Text = "Tracks";
            // 
            // TrackScrollBar
            // 
            this.TrackScrollBar.LargeChange = 1;
            this.TrackScrollBar.Location = new System.Drawing.Point(607, 16);
            this.TrackScrollBar.Name = "TrackScrollBar";
            this.TrackScrollBar.Size = new System.Drawing.Size(23, 256);
            this.TrackScrollBar.TabIndex = 0;
            this.TrackScrollBar.ValueChanged += new System.EventHandler(this.TrackScrollBar_ValueChanged);
            // 
            // lDiscInfo
            // 
            this.lDiscInfo.AutoSize = true;
            this.lDiscInfo.Location = new System.Drawing.Point(105, 12);
            this.lDiscInfo.Name = "lDiscInfo";
            this.lDiscInfo.Size = new System.Drawing.Size(0, 13);
            this.lDiscInfo.TabIndex = 4;
            // 
            // bWriteDiscTitle
            // 
            this.bWriteDiscTitle.Location = new System.Drawing.Point(608, 36);
            this.bWriteDiscTitle.Name = "bWriteDiscTitle";
            this.bWriteDiscTitle.Size = new System.Drawing.Size(75, 23);
            this.bWriteDiscTitle.TabIndex = 3;
            this.bWriteDiscTitle.Text = "Write";
            this.bWriteDiscTitle.UseVisualStyleBackColor = true;
            this.bWriteDiscTitle.Click += new System.EventHandler(this.bWriteDiscTitle_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Disc title:";
            // 
            // bReadTOC
            // 
            this.bReadTOC.Location = new System.Drawing.Point(14, 7);
            this.bReadTOC.Name = "bReadTOC";
            this.bReadTOC.Size = new System.Drawing.Size(75, 23);
            this.bReadTOC.TabIndex = 1;
            this.bReadTOC.Text = "Read TOC";
            this.bReadTOC.UseVisualStyleBackColor = true;
            this.bReadTOC.Click += new System.EventHandler(this.bReadTOC_Click);
            // 
            // tbDiscTitle
            // 
            this.tbDiscTitle.Location = new System.Drawing.Point(78, 38);
            this.tbDiscTitle.Name = "tbDiscTitle";
            this.tbDiscTitle.Size = new System.Drawing.Size(524, 20);
            this.tbDiscTitle.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.bDeleteAfter);
            this.tabPage2.Controls.Add(this.bDeleteBefore);
            this.tabPage2.Controls.Add(this.bDivideHere);
            this.tabPage2.Controls.Add(this.bDivide);
            this.tabPage2.Controls.Add(this.lCut);
            this.tabPage2.Controls.Add(this.sbCut);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(689, 364);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Cut & Edit";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // bDeleteAfter
            // 
            this.bDeleteAfter.Location = new System.Drawing.Point(409, 171);
            this.bDeleteAfter.Name = "bDeleteAfter";
            this.bDeleteAfter.Size = new System.Drawing.Size(75, 51);
            this.bDeleteAfter.TabIndex = 5;
            this.bDeleteAfter.Text = "Delete AFTER this point";
            this.bDeleteAfter.UseVisualStyleBackColor = true;
            this.bDeleteAfter.Visible = false;
            this.bDeleteAfter.Click += new System.EventHandler(this.bDeleteAfter_Click);
            // 
            // bDeleteBefore
            // 
            this.bDeleteBefore.Location = new System.Drawing.Point(220, 171);
            this.bDeleteBefore.Name = "bDeleteBefore";
            this.bDeleteBefore.Size = new System.Drawing.Size(75, 51);
            this.bDeleteBefore.TabIndex = 4;
            this.bDeleteBefore.Text = "Delete BEFORE this point";
            this.bDeleteBefore.UseVisualStyleBackColor = true;
            this.bDeleteBefore.Visible = false;
            this.bDeleteBefore.Click += new System.EventHandler(this.bDeleteBefore_Click);
            // 
            // bDivideHere
            // 
            this.bDivideHere.Location = new System.Drawing.Point(315, 185);
            this.bDivideHere.Name = "bDivideHere";
            this.bDivideHere.Size = new System.Drawing.Size(75, 23);
            this.bDivideHere.TabIndex = 3;
            this.bDivideHere.Text = "Divide here";
            this.bDivideHere.UseVisualStyleBackColor = true;
            this.bDivideHere.Visible = false;
            this.bDivideHere.Click += new System.EventHandler(this.bDivideHere_Click);
            // 
            // bDivide
            // 
            this.bDivide.Location = new System.Drawing.Point(315, 43);
            this.bDivide.Name = "bDivide";
            this.bDivide.Size = new System.Drawing.Size(75, 23);
            this.bDivide.TabIndex = 2;
            this.bDivide.Text = "Divide";
            this.bDivide.UseVisualStyleBackColor = true;
            this.bDivide.Click += new System.EventHandler(this.bDivide_Click);
            // 
            // lCut
            // 
            this.lCut.AutoSize = true;
            this.lCut.Location = new System.Drawing.Point(345, 96);
            this.lCut.Name = "lCut";
            this.lCut.Size = new System.Drawing.Size(13, 13);
            this.lCut.TabIndex = 1;
            this.lCut.Text = "0";
            this.lCut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sbCut
            // 
            this.sbCut.Location = new System.Drawing.Point(28, 114);
            this.sbCut.Maximum = 127;
            this.sbCut.Minimum = -127;
            this.sbCut.Name = "sbCut";
            this.sbCut.Size = new System.Drawing.Size(628, 27);
            this.sbCut.TabIndex = 0;
            this.sbCut.ValueChanged += new System.EventHandler(this.sbCut_ValueChanged);
            // 
            // bPlay
            // 
            this.bPlay.Location = new System.Drawing.Point(384, 25);
            this.bPlay.Name = "bPlay";
            this.bPlay.Size = new System.Drawing.Size(35, 23);
            this.bPlay.TabIndex = 4;
            this.bPlay.Text = ">";
            this.bPlay.UseVisualStyleBackColor = true;
            this.bPlay.Click += new System.EventHandler(this.bPlay_Click);
            // 
            // bStop
            // 
            this.bStop.Location = new System.Drawing.Point(343, 25);
            this.bStop.Name = "bStop";
            this.bStop.Size = new System.Drawing.Size(35, 23);
            this.bStop.TabIndex = 5;
            this.bStop.Text = "[_]";
            this.bStop.UseVisualStyleBackColor = true;
            this.bStop.Click += new System.EventHandler(this.bStop_Click);
            // 
            // bPause
            // 
            this.bPause.Location = new System.Drawing.Point(425, 25);
            this.bPause.Name = "bPause";
            this.bPause.Size = new System.Drawing.Size(35, 23);
            this.bPause.TabIndex = 6;
            this.bPause.Text = "| |";
            this.bPause.UseVisualStyleBackColor = true;
            this.bPause.Click += new System.EventHandler(this.bPause_Click);
            // 
            // bFFwd
            // 
            this.bFFwd.Location = new System.Drawing.Point(528, 24);
            this.bFFwd.Name = "bFFwd";
            this.bFFwd.Size = new System.Drawing.Size(35, 23);
            this.bFFwd.TabIndex = 7;
            this.bFFwd.Text = "> >";
            this.bFFwd.UseVisualStyleBackColor = true;
            this.bFFwd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bFFwd_MouseDown);
            this.bFFwd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bFFwd_MouseUp);
            // 
            // bRew
            // 
            this.bRew.Location = new System.Drawing.Point(487, 24);
            this.bRew.Name = "bRew";
            this.bRew.Size = new System.Drawing.Size(35, 23);
            this.bRew.TabIndex = 8;
            this.bRew.Text = "< <";
            this.bRew.UseVisualStyleBackColor = true;
            this.bRew.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bRew_MouseDown);
            this.bRew.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bRew_MouseUp);
            // 
            // bPrevTrack
            // 
            this.bPrevTrack.Location = new System.Drawing.Point(362, 53);
            this.bPrevTrack.Name = "bPrevTrack";
            this.bPrevTrack.Size = new System.Drawing.Size(35, 23);
            this.bPrevTrack.TabIndex = 10;
            this.bPrevTrack.Text = "|< <";
            this.bPrevTrack.UseVisualStyleBackColor = true;
            this.bPrevTrack.Click += new System.EventHandler(this.bPrevTrack_Click);
            // 
            // bNextTrack
            // 
            this.bNextTrack.Location = new System.Drawing.Point(403, 53);
            this.bNextTrack.Name = "bNextTrack";
            this.bNextTrack.Size = new System.Drawing.Size(35, 23);
            this.bNextTrack.TabIndex = 9;
            this.bNextTrack.Text = "> >|";
            this.bNextTrack.UseVisualStyleBackColor = true;
            this.bNextTrack.Click += new System.EventHandler(this.bNextTrack_Click);
            // 
            // bRecord
            // 
            this.bRecord.ForeColor = System.Drawing.Color.Red;
            this.bRecord.Location = new System.Drawing.Point(583, 24);
            this.bRecord.Name = "bRecord";
            this.bRecord.Size = new System.Drawing.Size(35, 23);
            this.bRecord.TabIndex = 11;
            this.bRecord.Text = "O";
            this.bRecord.UseVisualStyleBackColor = true;
            this.bRecord.Click += new System.EventHandler(this.bRecord_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(634, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Undo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lTrackInfo
            // 
            this.lTrackInfo.AutoSize = true;
            this.lTrackInfo.Location = new System.Drawing.Point(331, 6);
            this.lTrackInfo.Name = "lTrackInfo";
            this.lTrackInfo.Size = new System.Drawing.Size(30, 13);
            this.lTrackInfo.TabIndex = 13;
            this.lTrackInfo.Text = "Time";
            // 
            // bEject
            // 
            this.bEject.Location = new System.Drawing.Point(634, 53);
            this.bEject.Name = "bEject";
            this.bEject.Size = new System.Drawing.Size(75, 23);
            this.bEject.TabIndex = 14;
            this.bEject.Text = "Eject";
            this.bEject.UseVisualStyleBackColor = true;
            this.bEject.Click += new System.EventHandler(this.bEject_Click);
            // 
            // bWriteTOC
            // 
            this.bWriteTOC.Location = new System.Drawing.Point(512, 9);
            this.bWriteTOC.Name = "bWriteTOC";
            this.bWriteTOC.Size = new System.Drawing.Size(75, 23);
            this.bWriteTOC.TabIndex = 6;
            this.bWriteTOC.Text = "Write TOC";
            this.bWriteTOC.UseVisualStyleBackColor = true;
            this.bWriteTOC.Click += new System.EventHandler(this.bWriteTOC_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 455);
            this.Controls.Add(this.bEject);
            this.Controls.Add(this.lTrackInfo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bRecord);
            this.Controls.Add(this.bPrevTrack);
            this.Controls.Add(this.bNextTrack);
            this.Controls.Add(this.bRew);
            this.Controls.Add(this.bFFwd);
            this.Controls.Add(this.bPause);
            this.Controls.Add(this.bStop);
            this.Controls.Add(this.bPlay);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.bDisconnect);
            this.Controls.Add(this.bConnect);
            this.Controls.Add(this.ComPortSelector);
            this.Name = "Form1";
            this.Text = "MD";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.gTracks.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComPortSelector;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.Button bDisconnect;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button bWriteDiscTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bReadTOC;
        private System.Windows.Forms.TextBox tbDiscTitle;
        private System.Windows.Forms.Button bPlay;
        private System.Windows.Forms.Button bStop;
        private System.Windows.Forms.Button bPause;
        private System.Windows.Forms.Button bFFwd;
        private System.Windows.Forms.Button bRew;
        private System.Windows.Forms.Button bPrevTrack;
        private System.Windows.Forms.Button bNextTrack;
        private System.Windows.Forms.Button bRecord;
        private System.Windows.Forms.Label lDiscInfo;
        private System.Windows.Forms.GroupBox gTracks;
        private System.Windows.Forms.VScrollBar TrackScrollBar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lCut;
        private System.Windows.Forms.HScrollBar sbCut;
        private System.Windows.Forms.Button bDivide;
        private System.Windows.Forms.Button bDeleteAfter;
        private System.Windows.Forms.Button bDeleteBefore;
        private System.Windows.Forms.Button bDivideHere;
        private System.Windows.Forms.Label lTrackInfo;
        private System.Windows.Forms.Button bEject;
        private System.Windows.Forms.Button bWriteTOC;
    }
}

