﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using MD;
namespace MD_Application
{
    public partial class Form1 : Form
    {
        MDSE11 MD;

        bool MoveTrack = false;
        int TrackToMove = 0;
        byte Track = 0, Minute = 0, Second = 0;

        public class TrackData
        {
            public int TrackNo;
            public string Name;
            public int Min;
            public int Sec;
        }

        public void ElapsedTimeCallback(byte T, byte M, byte S)
        {
            Track = T;
            Minute = M;
            Second = S;
        }

        List<TrackData> Tracks = new List<TrackData>();

        List<Button> TrackButtons = new List<Button>();
        List<TextBox> TrackNameTextBoxes = new List<TextBox>();
        List<Button> WriteTrackNameButtons = new List<Button>();
        List<Button> DeleteTrackButtons = new List<Button>();
        List<Button> MoveTrackButtons = new List<Button>();
        List<Button> JoinTrackButtons = new List<Button>();


        public Form1()
        {
            InitializeComponent();
            PopulateComPortList();

            for(int i = 0; i < 10; i++)
            {
                Button B = new Button();
                B.Parent = gTracks;
                B.Location = new Point(10, (i * 25) + 20);
                B.Width = 30;
                B.Tag = i;
                B.Click += bTrackPlayClick;
                B.Visible = false;
                TrackButtons.Add(B);
                

                TextBox TB = new TextBox();
                TB.Parent = gTracks;
                TB.Location = new Point(45, (i * 25) + 21);
                TB.Width = 150;
                TB.Visible = false;
                TrackNameTextBoxes.Add(TB);

                B = new Button();
                B.Parent = gTracks;
                B.Location = new Point(200, (i * 25) + 20);
                B.Width = 40;
                B.Tag = i;
                B.Click += bWriteTrackNameClick;
                B.Text = "Write";
                B.Visible = false;
                WriteTrackNameButtons.Add(B);

                B = new Button();
                B.Parent = gTracks;
                B.Location = new Point(245, (i * 25) + 20);
                B.Width = 80;
                B.Tag = i;
                B.Click += bDeleteTrackClick;
                B.Visible = false;
                DeleteTrackButtons.Add(B);

                B = new Button();
                B.Parent = gTracks;
                B.Location = new Point(330, (i * 25) + 20);
                B.Width = 80;
                B.Tag = i;
                B.Click += bMoveTrackClick;
                B.Visible = false;
                MoveTrackButtons.Add(B);

                B = new Button();
                B.Parent = gTracks;
                B.Location = new Point(415, (i * 25) + 32);
                B.Width = 50;
                B.Tag = i;
                B.Click += bJoinButtonClick;
                B.Visible = false;
                B.Text = "Join";
                JoinTrackButtons.Add(B);
            }

        }

        public void UpdateTrackScreenList()
        {
            if (Tracks.Count > 10)
            {
                TrackScrollBar.Visible = true;
                TrackScrollBar.Minimum = 0;
                TrackScrollBar.Maximum = Tracks.Count - 10;
            }
            else
            {
                TrackScrollBar.Visible = false;
                TrackScrollBar.Value = 0;
            }

            for (int i = 0; i < 10; i++)
            {
                int TNr = i + TrackScrollBar.Value;
                if (TNr < Tracks.Count)
                {
                    TrackButtons[i].Text = (TNr + 1).ToString();
                    TrackButtons[i].Visible = true;

                    TrackNameTextBoxes[i].Text = Tracks[TNr].Name;
                    TrackNameTextBoxes[i].Visible = true;

                    WriteTrackNameButtons[i].Visible = true;

                    DeleteTrackButtons[i].Text = "Delete " + (TNr + 1);
                    DeleteTrackButtons[i].Visible = true;

                    MoveTrackButtons[i].Text = "Move " + (TNr + 1) + " to...";
                    MoveTrackButtons[i].Visible = true;

                }
                else
                {
                    TrackButtons[i].Visible = false;
                    TrackNameTextBoxes[i].Visible = false;

                    WriteTrackNameButtons[i].Visible = false;

                    DeleteTrackButtons[i].Visible = false;
                    MoveTrackButtons[i].Visible = false;
                }
            }

            for (int i = 0; i < 9; i++)
            {
                int TNr = i + TrackScrollBar.Value + 1;
                if (TNr < Tracks.Count)
                {
                    JoinTrackButtons[i].Visible = true;
                }
                else
                {
                    JoinTrackButtons[i].Visible = false;
                }
            }
        }

        public void PopulateComPortList()
        {
            string[] List = System.IO.Ports.SerialPort.GetPortNames();
            ComPortSelector.Items.Clear();
            foreach (string s in List) ComPortSelector.Items.Add(s);

            if(System.IO.File.Exists("ComPort.txt") == true) ComPortSelector.Text = System.IO.File.ReadAllText("ComPort.txt");
        }

        private void bTrackPlayClick(object sender, EventArgs e)
        {
            int line = (int)((Button)sender).Tag;
            int Track = TrackScrollBar.Value + line + 1;

            if (MoveTrack == false)
            {
                MDSE11.StatusData SD = MD.StatusRequest();
                if (SD.State != MDSE11.State_.STOP) MD.Stop();

                MD.Track_Play(Track);
            }
            else //Move track
            {
                MD.Move(TrackToMove, Track);
                MoveTrack = false;

                for (int i = 0; i < 10; i++)
                {
                    TrackButtons[i].BackColor = Color.LightGray;
                }

                //MDSE11.TOCData TD = MD.TocDataRequest();
                //UpdateTracksList(TD);

                TrackData ToMove = Tracks[TrackToMove - 1];

                Tracks.RemoveAt(TrackToMove - 1);
                Tracks.Insert(Track - 1, ToMove);
                UpdateTrackScreenList();
            }
        }

        private void bWriteTrackNameClick(object sender, EventArgs e)
        {
            int line = (int)((Button)sender).Tag;
            int Track = TrackScrollBar.Value + line + 1;
            string Name = TrackNameTextBoxes[line].Text;

            MDSE11.StatusData SD = MD.StatusRequest();
            if (SD.State != MDSE11.State_.STOP) MD.Stop();

            MD.TrackNameWrite(Track, Name);

            Tracks[Track - 1].Name = Name;
        }

        private void bDeleteTrackClick(object sender, EventArgs e)
        {
            int line = (int)((Button)sender).Tag;
            int Track = TrackScrollBar.Value + line + 1;
            
            MDSE11.StatusData SD = MD.StatusRequest();
            if (SD.State != MDSE11.State_.STOP) MD.Stop();

            MD.Erase(Track);

            //MDSE11.TOCData TD = MD.TocDataRequest();
            //UpdateTracksList(TD);
            Tracks.RemoveAt(Track - 1);
            UpdateTrackScreenList();
            UpdateTrackScreenList();
        }

        private void bJoinButtonClick(object sender, EventArgs e)
        {
            int line = (int)((Button)sender).Tag;
            int Track = TrackScrollBar.Value + line + 1;

            MDSE11.StatusData SD = MD.StatusRequest();
            if (SD.State != MDSE11.State_.STOP) MD.Stop();

            MD.CombineModeReqE11(Track);
            System.Threading.Thread.Sleep(10000);
            MD.CombineReqE11(Track + 1);

            MDSE11.TOCData TD = MD.TocDataRequest();
            UpdateTracksList(TD);
            UpdateTrackScreenList();
        }

        private void bMoveTrackClick(object sender, EventArgs e)
        {
            int line = (int)((Button)sender).Tag;
            int Track = TrackScrollBar.Value + line + 1;

            MDSE11.StatusData SD = MD.StatusRequest();
            if (SD.State != MDSE11.State_.STOP) MD.Stop();

            MoveTrack = true;
            TrackToMove = Track;

            for(int i = 0; i < 10; i++)
            {
                TrackButtons[i].BackColor = Color.Green;
            }
        }

        private void bConnect_Click(object sender, EventArgs e)
        {
            if (MD != null) MD.DisConnect();

            try
            {
                MD = new MDSE11(ComPortSelector.Text);
            }
            catch
            {
                MessageBox.Show("Invalid COM port.");
                return;
            }

            MD.RemoteMode(0);
            if (MD.RemoteMode(1) == -1)
            {
                MessageBox.Show("There is no player answering on " + ComPortSelector.Text + ".");
                return;
            }
            else
            {
                tabControl1.Visible = true;
                System.IO.File.WriteAllText("ComPort.txt", ComPortSelector.Text);

                string Name = MD.ModelNameRequest();
                this.Text = "Talking to a friendly " + Name + ".";

                MD.ElapsedTime = ElapsedTimeCallback;
                MD.ElapsedTimeOnOff(1);
            }
        }

        private void bDisconnect_Click(object sender, EventArgs e)
        {
            if (MD != null) MD.DisConnect();
            tabControl1.Visible = false;

            this.Text = "Alone again....";
        }

        private void bReadTOC_Click(object sender, EventArgs e)
        {
            tbDiscTitle.Text = MD.DiscNameRequest();

            MDSE11.TOCData TD = MD.TocDataRequest();
            lDiscInfo.Text = "Disc contains" + TD.LastTrackNo + " tracks. " + TD.RecordedMin + " minutes and " + TD.RecordedSec + " seconds recorded.";

            UpdateTracksList(TD);
            UpdateTrackScreenList();

        }

        public void UpdateTracksList(MDSE11.TOCData TD)
        {
            Tracks.Clear();
            for (int i = 0; i < TD.LastTrackNo; i++)
            {
                TrackData Track = new TrackData();
                Track.Name = MD.TrackNameRequest(i + 1);
                Track.TrackNo = i + 1;

                MDSE11.TimeData Time = MD.TrackTimeRequest(i + 1);
                Track.Min = Time.Min;
                Track.Sec = Time.Sec;

                Tracks.Add(Track);
            }
        }

        private void bWriteDiscTitle_Click(object sender, EventArgs e)
        {
            MDSE11.StatusData SD = MD.StatusRequest();
            if (SD.State != MDSE11.State_.STOP) MD.Stop();

            MD.DiscNameWrite(tbDiscTitle.Text);
        }

        private void bPlay_Click(object sender, EventArgs e)
        {
            if(MD != null) MD.Play();
        }

        private void bStop_Click(object sender, EventArgs e)
        {
            if (MD != null) MD.Stop();
        }

        private void bPause_Click(object sender, EventArgs e)
        {
            if (MD != null) MD.PauseOn();
        }

        private void bFFwd_MouseUp(object sender, MouseEventArgs e)
        {
            if (MD != null) MD.FF_REW_OFF();
        }

        private void bFFwd_MouseDown(object sender, MouseEventArgs e)
        {
            if (MD != null) MD.FF();
        }

        private void bRew_MouseDown(object sender, MouseEventArgs e)
        {
            if (MD != null) MD.Rew();
        }

        private void bRew_MouseUp(object sender, MouseEventArgs e)
        {
            if (MD != null) MD.FF_REW_OFF();
        }

        private void bNextTrack_Click(object sender, EventArgs e)
        {
            if (MD != null) MD.Next_Track();
        }

        private void bPrevTrack_Click(object sender, EventArgs e)
        {
            if (MD != null) MD.Prev_Track();
        }

        private void bRecord_Click(object sender, EventArgs e)
        {
            if (MD != null) MD.Rec();
        }

        private void TrackScrollBar_ValueChanged(object sender, EventArgs e)
        {
            UpdateTrackScreenList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(MD != null)
            {
                MD.Undo();
            }
        }

        private void sbCut_ValueChanged(object sender, EventArgs e)
        {
            lCut.Text = sbCut.Value.ToString();
            MD.DivideAdjust(sbCut.Value);
        }

        private void bDivide_Click(object sender, EventArgs e)
        {
            sbCut.Value = 0;

            MD.DivideModeReq();

            bDivideHere.Visible = true;
            bDeleteAfter.Visible = true;
            bDeleteBefore.Visible = true;
        }

        private void bDeleteAfter_Click(object sender, EventArgs e)
        {
            byte CurrentTrack = Track;
            MD.DivideReq();
            MD.Erase(CurrentTrack + 1);
            bDivideHere.Visible = false;
            bDeleteAfter.Visible = false;
            bDeleteBefore.Visible = false;
        }

        private void bEject_Click(object sender, EventArgs e)
        {
            MD.Eject();
        }

        private void bWriteTOC_Click(object sender, EventArgs e)
        {
            MDSE11.StatusData SD = MD.StatusRequest();
            if (SD.State != MDSE11.State_.STOP) MD.Stop();

            MD.DiscNameWrite(tbDiscTitle.Text);

            for (int Track = 1; Track <= Tracks.Count; Track++)
            {
                MD.TrackNameWrite(Track, Tracks[Track - 1].Name);
            }
        }

        private void bDivideHere_Click(object sender, EventArgs e)
        {
            MD.DivideReq();

            bDivideHere.Visible = false;
            bDeleteAfter.Visible = false;
            bDeleteBefore.Visible = false;
        }

        private void bDeleteBefore_Click(object sender, EventArgs e)
        {
            byte CurrentTrack = Track;
            MD.DivideReq();
            MD.Erase(CurrentTrack);
            bDivideHere.Visible = false;
            bDeleteAfter.Visible = false;
            bDeleteBefore.Visible = false;
        }
    }
}
